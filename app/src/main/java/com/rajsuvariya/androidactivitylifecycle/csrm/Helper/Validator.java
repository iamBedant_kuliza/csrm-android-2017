package com.rajsuvariya.androidactivitylifecycle.csrm.Helper;

import android.text.TextUtils;
import android.util.Patterns;

/**
 * Created by kuliza-319 on 25/1/17.
 */

public class Validator {


    public static boolean isValidEmail(String email){
        if (TextUtils.isEmpty(email)) {
            return false;
        } else {
            return Patterns.EMAIL_ADDRESS.matcher(email).matches();
        }
    }

    public static boolean isValidMobileNumber(String mobile){
        if (TextUtils.isEmpty(mobile) || mobile.length()!=10) {
            return false;
        }
        else{
            return mobile.matches("[0-9]+");
        }
    }

    public static boolean isValidPinCode(String mobile){
        if (TextUtils.isEmpty(mobile) || mobile.length()!=6) {
            return false;
        }
        else{
            return mobile.matches("[0-9]+");
        }
    }

    public static boolean isFirstNameValid(String name){
        if(TextUtils.isEmpty(name) || name.length()<2){
            return false;
        }
        else{
            return true;
//                    name.matches("^([a-zA-Z]+\\s?)+$");
        }
    }

    public static boolean isLastNameValid(String name){
        if(TextUtils.isEmpty(name)){
            return false;
        }
        else{
            return true;
//            return name.matches("^([a-zA-Z])+$");
        }
    }

//   strong ^(?=.*[A-Z])(?=.*[!@#$&*])(?=.*[0-9])(?=.*[a-z]).{8,}$
// medium  ^(((?=.*[a-z])(?=.*[A-Z]))|((?=.*[a-z])(?=.*[0-9]))|((?=.*[A-Z])(?=.*[0-9])))(?=.{6,})
    public static boolean isPasswordValid(String pass){
        if (TextUtils.isEmpty(pass)||pass.length()<5){
            return false;
        }
        else {
            return true;
        }
    }

    public static boolean isAddressFirstLineValid(String addressLine1){
        if(TextUtils.isEmpty(addressLine1)||addressLine1.length()<3){
            return false;
        }
        else{
            return true;
        }
    }

    public static boolean isAddressLandmarkValid(String addressLine1){
        if(TextUtils.isEmpty(addressLine1)||addressLine1.length()<3){
            return false;
        }
        else{
            return true;
        }
    }

    public static boolean isAddressCityValid(String addressLine1){
        if(TextUtils.isEmpty(addressLine1)||addressLine1.length()<3){
            return false;
        }
        else{
            return true;
        }
    }
}
