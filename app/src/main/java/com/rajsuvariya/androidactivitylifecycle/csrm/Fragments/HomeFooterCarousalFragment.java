package com.rajsuvariya.androidactivitylifecycle.csrm.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.rajsuvariya.androidactivitylifecycle.csrm.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HomeFooterCarousalFragment extends Fragment {

    private int mImageId;
    private String mButtonText;
    private String mBannerTextTitle;
    private String mBannerTextDescription;

    @BindView(R.id.home_footer_carousal_background_image) View mViewBackground;
    @BindView(R.id.home_footer_fragment_text_title) TextView mTextViewTitle;
    @BindView(R.id.home_footer_fragment_text_description) TextView mTextViewDescription;
    @BindView(R.id.custombutton_layout_button) Button mButtonKnowMore;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_home_footer_carousal, container, false);
        ButterKnife.bind(this, rootView);
        mViewBackground.setBackgroundResource(mImageId);
        mButtonKnowMore.setText(mButtonText);
        mTextViewTitle.setText(mBannerTextTitle);
        mTextViewDescription.setText(mBannerTextDescription);
        return rootView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    public void setData(int imageId, String buttonText, String bannerTextTitle, String bannerTextDescription){
        this.mImageId = imageId;
        this.mButtonText = buttonText;
        this.mBannerTextTitle = bannerTextTitle;
        this.mBannerTextDescription = bannerTextDescription;
    }

}
