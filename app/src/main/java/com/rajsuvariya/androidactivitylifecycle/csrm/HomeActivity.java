package com.rajsuvariya.androidactivitylifecycle.csrm;

import android.graphics.drawable.ColorDrawable;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.gigamole.infinitecycleviewpager.HorizontalInfiniteCycleViewPager;
import com.rajsuvariya.androidactivitylifecycle.csrm.Adapter.FragmentAdapter;
import com.rajsuvariya.androidactivitylifecycle.csrm.Adapter.HorizontalPagerAdapter;
import com.rajsuvariya.androidactivitylifecycle.csrm.Fragments.HomeHeaderFragment;
import com.rajsuvariya.androidactivitylifecycle.csrm.Helper.Message;
import com.viewpagerindicator.CirclePageIndicator;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class HomeActivity extends AppCompatActivity {

    @BindView(R.id.home_header_viewpager) ViewPager mViewPager;
    @BindView(R.id.home_viewpager_indicator) CirclePageIndicator mViewPagerIndicator;
    @BindView(R.id.home_cardview_earn) CardView mCardViewEarnPoints;
    @BindView(R.id.home_cardview_redeem) CardView mCardViewRedeemPoints;
    @BindView(R.id.home_header_viewpager_prev_button) ImageView mButtonPrevViewPager;
    @BindView(R.id.home_header_viewpager_next_button) ImageView mButtonNextViewPager;
    @BindView(R.id.home_bottom_carousal) HorizontalInfiniteCycleViewPager mCarousalView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        // Binding views
        ButterKnife.bind(this);

        // Setting Actionbar with the home icon and logo
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.custom_actionbar);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.endeavour)));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // Setting Custom Buttons labels in cardviews
        Button btEarnPoint = ((Button) mCardViewEarnPoints.findViewById(R.id.custombutton_layout_button));
        btEarnPoint.setText(getString(R.string.button_know_more_label));
        btEarnPoint.setOnClickListener(new View.OnClickListener() {
               @Override
               public void onClick(View v) {
                   Message.showShortToast(getApplicationContext(), "Earn Points Know More");
               }
           }
        );
        Button btRedeemPoints = ((Button) mCardViewRedeemPoints.findViewById(R.id.custombutton_layout_button));
        btRedeemPoints.setText(getString(R.string.button_know_more_label));
        btRedeemPoints.setOnClickListener(new View.OnClickListener() {
               @Override
               public void onClick(View v) {
                   Message.showShortToast(getApplicationContext(), "Redeem Points Know More");
               }
            }
        );

        // Setting up carousal (at bottom)
        setupCarousal();

        // Setting up viewpager (at top)
        setupViewPager();

        // Setting up circle page indicator
        setupCricleViewPagerIndicator();


        
    }

    // onClickListener for previous button on viewpager
    @OnClick(R.id.home_header_viewpager_prev_button)
    public void prevHeaderButtonClick(){
        if (mViewPager.getCurrentItem()==0){
            mViewPager.setCurrentItem(mViewPager.getAdapter().getCount()-1, true);
        }
        else {
            mViewPager.setCurrentItem((mViewPager.getCurrentItem() - 1) % mViewPager.getAdapter().getCount(), true);
        }
    }

    // onClickListener for next button on viewpager
    @OnClick(R.id.home_header_viewpager_next_button)
    public void nextHeaderButtonClick(){
        mViewPager.setCurrentItem((mViewPager.getCurrentItem()+1)% mViewPager.getAdapter().getCount(), true);
    }

    // onClickListener for previous button on carousal
    @OnClick(R.id.home_footer_carousal_prev_button)
    public void prevFooterButtonClick(){
        if (mCarousalView.getCurrentItem()==0){
            mCarousalView.setCurrentItem(mCarousalView.getAdapter().getCount()-1, true);
        }
        else {
            mCarousalView.setCurrentItem((mCarousalView.getCurrentItem()-1)% mCarousalView.getAdapter().getCount(), true);
        }
    }

    // onClickListener for next button on carousal
    @OnClick(R.id.home_footer_carousal_next_button)
    public void nextFooterButtonClick(){
        mCarousalView.setCurrentItem((mCarousalView.getCurrentItem()+1)% mCarousalView.getAdapter().getCount(), true);
    }

    /**
     *  Create Fragment Adapter
     *  Add fragments into adapter
     *  set adapter to viewpager
     *  add on page change listener to control visibility of the next and prev button
     */
    private void setupViewPager() {
        FragmentAdapter adapter = new FragmentAdapter(getSupportFragmentManager());
        HomeHeaderFragment first = new HomeHeaderFragment();
        HomeHeaderFragment second = new HomeHeaderFragment();
        HomeHeaderFragment third = new HomeHeaderFragment();
        HomeHeaderFragment fourth = new HomeHeaderFragment();
        HomeHeaderFragment fifth = new HomeHeaderFragment();

        first.setData(R.drawable.home_header_viewpager_banner, getString(R.string.button_know_more_label), getString(R.string.redeem_offer_label1));
        second.setData(R.drawable.image1, getString(R.string.button_know_more_label), getString(R.string.redeem_offer_label2));
        third.setData(R.drawable.image2, getString(R.string.button_know_more_label), getString(R.string.redeem_offer_label3));
        fourth.setData(R.drawable.image3, getString(R.string.button_know_more_label), getString(R.string.redeem_offer_label1));
        fifth.setData(R.drawable.background_image_registration_page, getString(R.string.button_know_more_label), getString(R.string.redeem_offer_label2));

        adapter.addFragment(first, "First");
        adapter.addFragment(second, "Second");
        adapter.addFragment(third, "Third");
        adapter.addFragment(fourth, "Fourth");
        adapter.addFragment(fifth, "Fifth");

        mViewPager.setAdapter(adapter);

        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                if(position!=0){
                    mButtonPrevViewPager.setVisibility(View.VISIBLE);
                }
                else{
                    mButtonPrevViewPager.setVisibility(View.GONE);
                }
                if(position!=(mViewPager.getAdapter().getCount()-1)){
                    mButtonNextViewPager.setVisibility(View.VISIBLE);
                }
                else{
                    mButtonNextViewPager.setVisibility(View.GONE);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    /**
     *  Create Pager Adapter
     *  add data into adapter
     *  set adapter to carousal
     */
    private void setupCarousal() {
        HorizontalPagerAdapter adapter = new HorizontalPagerAdapter(this, false);
        adapter.addData(R.drawable.image4, getString(R.string.button_know_more_label), getString(R.string.redeem_option_gym_membership_title), getString(R.string.redeem_option_gym_membership_description));
        adapter.addData(R.drawable.image1, getString(R.string.button_know_more_label), getString(R.string.redeem_option_basketball_membership_title),getString(R.string.redeem_option_basketball_membership_description));
        adapter.addData(R.drawable.image2, getString(R.string.button_know_more_label), getString(R.string.redeem_option_cricket_membership_title),getString(R.string.redeem_option_cricket_membership_description));
        adapter.addData(R.drawable.image3, getString(R.string.button_know_more_label), getString(R.string.redeem_option_football_membership_title),getString(R.string.redeem_option_football_membership_description));
        adapter.addData(R.drawable.background_image_registration_page, getString(R.string.button_know_more_label), getString(R.string.redeem_option_danceclub_membership_title),getString(R.string.redeem_option_danceclub_membership_description));
        mCarousalView.setAdapter(adapter);
    }

    /**
     *  Add viewpager to indicator
     *  Change the radius of the circle
     */
    private void setupCricleViewPagerIndicator() {
        mViewPagerIndicator.setViewPager(mViewPager);
        final float density = getResources().getDisplayMetrics().density;
        mViewPagerIndicator.setRadius(5 * density);
    }


}
