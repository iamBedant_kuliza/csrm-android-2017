package com.rajsuvariya.androidactivitylifecycle.csrm;

import android.graphics.drawable.ColorDrawable;
import android.support.design.widget.TabLayout;
import android.support.design.widget.TextInputLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;

import com.rajsuvariya.androidactivitylifecycle.csrm.Adapter.FragmentAdapter;
import com.rajsuvariya.androidactivitylifecycle.csrm.Fragments.LoginFragment;
import com.rajsuvariya.androidactivitylifecycle.csrm.Fragments.SignUpFragment;
import com.rajsuvariya.androidactivitylifecycle.csrm.Helper.Message;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.main_viewpager) ViewPager mViewPager;
    @BindView(R.id.main_tablayout) TabLayout mTabLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Binding views with butterknife
        ButterKnife.bind(this);

        // Setting Actionbar with the home icon and logo
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.custom_actionbar);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.endeavour)));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // Setting up viewpager with the fragments
        setupViewPager();

        // Setting up mTabLayout with viewpager fragments titles
        mTabLayout.setupWithViewPager(mViewPager);

    }

    // adding fragments to adapter and seeting up the adapter to viewpager
    public void setupViewPager() {
        FragmentAdapter adapter = new FragmentAdapter(getSupportFragmentManager());
        adapter.addFragment(new LoginFragment(), "login");
        adapter.addFragment(new SignUpFragment(), "SignUp");
        mViewPager.setAdapter(adapter);
    }


    // Textwatcher class for implementing some actions when text in edit text changes
    public static class MyTextWatcher implements TextWatcher {

        TextInputLayout mTextInputLayout;

        public MyTextWatcher(TextInputLayout mTextInputLayout) {
            this.mTextInputLayout = mTextInputLayout;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        // On text change remove the error
        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            mTextInputLayout.setError("");
            mTextInputLayout.setErrorEnabled(false);
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()==android.R.id.home){
            Message.showShortToast(this, "Home Clicked!");
        }
        return super.onOptionsItemSelected(item);
    }
}


