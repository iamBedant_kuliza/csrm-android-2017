package com.rajsuvariya.androidactivitylifecycle.csrm.Adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.rajsuvariya.androidactivitylifecycle.csrm.Helper.Message;
import com.rajsuvariya.androidactivitylifecycle.csrm.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kuliza-319 on 31/1/17.
 */

public class HorizontalPagerAdapter extends PagerAdapter {

    private Context mContext;
    private LayoutInflater mLayoutInflater;

    List<String> mBannerTitleList = new ArrayList<>();
    List<String> mBannerDescriptionList = new ArrayList<>();
    List<String> mButtonTextList = new ArrayList<>();
    List<Integer> mImageId = new ArrayList<>();

    public HorizontalPagerAdapter(final Context context, final boolean isTwoWay) {
        mContext = context;
        mLayoutInflater = LayoutInflater.from(context);
    }

    public void addData(int imageId, String buttonText, String bannerTextTitle, String bannerTextDescription){
        mImageId.add(imageId);
        mButtonTextList.add(buttonText);
        mBannerDescriptionList.add(bannerTextDescription);
        mBannerTitleList.add(bannerTextTitle);
    }

    @Override
    public int getCount() {
        return mImageId.size();
    }

    @Override
    public int getItemPosition(final Object object) {
        return POSITION_NONE;
    }

    @Override
    public Object instantiateItem(final ViewGroup container, final int position) {
        final View view;

        view = mLayoutInflater.inflate(R.layout.fragment_home_footer_carousal, container, false);
        final TextView tvTitle = (TextView) view.findViewById(R.id.home_footer_fragment_text_title);
        tvTitle.setText(mBannerTitleList.get(position));

        RelativeLayout relativeLayout = (RelativeLayout) view.findViewById(R.id.home_footer_carousal_background_image);
        relativeLayout.setBackgroundResource(mImageId.get(position));

        TextView tvDesc = (TextView) view.findViewById(R.id.home_footer_fragment_text_description);
        tvDesc.setText(mBannerDescriptionList.get(position));

        Button bt = (Button) view.findViewById(R.id.custombutton_layout_button);
        bt.setText(mButtonTextList.get(position));
        bt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Message.showShortToast(v.getContext(), tvTitle.getText().toString()+" Clicked !");
            }
        });

        container.addView(view);
        return view;
    }

    @Override
    public boolean isViewFromObject(final View view, final Object object) {
        return view.equals(object);
    }

    @Override
    public void destroyItem(final ViewGroup container, final int position, final Object object) {
        container.removeView((View) object);
    }
}